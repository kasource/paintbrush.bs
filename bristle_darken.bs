--[[
	bristle_darken.bs
]]

-- defined
-- brushTip={{n = stepNum, n = polygonNum, n = threshold of patch},...}
-- Index of brushTip begin to 1.
brushTip={{3, 5, 80}, {3, 3, 60}, {5, 5, 80}, {5, 3, 80}}
brushTip.n=_iob or 1

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"interval", 2, 100, 5},
		{"mix", 0, 100, 50},
		{"drag", 0, 100, 80},
		{"smooth", 1, 100, 50},
		{"mask-alpha", 0, 100, 0},
		{"soft-edge", 0, 1, 0},
		{"rnd-size", 0, 100, 0},
		{"flat", 0, 100, 0},
		{"angle", 0, 100, 0},
		{"press-alpha", 0, 100, 50}
	}

	pm.ja={
		"間隔", "混色", "色の引きずり", "補間数", "透明マスク",
		"ソフトエッジ", "ランダム-サイズ", "扁平率", "角度", "筆圧-透明度"
	}

	for i in ipairs(pm) do

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		_G[string.format('param%d', i)]=setParam(pm[i])
		pm[i].v=_G[string.format('bs_param%d', i)]()

	end

	default_size=setParam(50, 0)

	firstDraw=true
	lastPosX, lastPosY=0, 0

-- sub routine --
function rgba(...)
	local t={...}
	local r, g, b, a=t[1] or 0, t[2] or 0, t[3] or 0, t[4] or bs_opaque()*255
	return {r=r, g=g, b=b, a=a}
end

function getColor(rate, a)
	local rate=rate or 0
	local t={bs_forebg(1-math.min(math.max(rate, 0), 1))}
	if a then
		return rgba(t[1], t[2], t[3], math.min(math.max(a, 0), 255))
	else
		return rgba(t[1], t[2], t[3])
	end
end

function getCanvasColor(x, y, radius)

	local pGet=rgba(0, 0, 0, 0)
	local result=rgba(0, 0, 0, 0)

	local moveAngle=bs_atan(bs_dir())
	local pickNum=math.max(90*math.min(bs_width(), 20)/20, 4)
	local pickAngle=180/pickNum

	for i=0, pickNum do

		local cx=x+radius*math.cos(moveAngle+math.rad(pickAngle*i-90))
		local cy=y+radius*math.sin(moveAngle+math.rad(pickAngle*i-90))

		pGet.a=bs_pixel_get_alpha(cx, cy)

		if pGet.a > 0 then
			pGet.r, pGet.g, pGet.b=bs_pixel_get(cx, cy)
			result.r=result.r+pGet.r*pGet.a
			result.g=result.g+pGet.g*pGet.a
			result.b=result.b+pGet.b*pGet.a
			result.a=result.a+pGet.a
		end
	end

	if result.a > 0 then
		result.r=math.floor(result.r/result.a)
		result.g=math.floor(result.g/result.a)
		result.b=math.floor(result.b/result.a)
		result.a=math.floor(result.a/(pickNum+1))
	end

	return result
end

average={
	init=function ()
		local obj={array={}, divSum=0, divWeight=0}
		setmetatable(obj, {__index=average})
		return obj
	end
}

function average:put(num, value)

	if #self.array == 0 then
		for i=1, math.max(num, 1) do
			self.array[i]=value
		end
	else
		table.insert(self.array, value)
		table.remove(self.array, 1)
	end
end

function average:sum(mode, weight)

	if mode < 0 or mode > 2 then
		return nil
	end

	self.divSum, self.divWeight=0, 0

	for i, v in ipairs(self.array) do

		local _weight=1

		if mode == 1 then
			_weight=i
		elseif mode == 2 then
			_weight=i^weight
		end

		self.divSum=self.divSum+v*_weight
		self.divWeight=self.divWeight+_weight
	end

	self.divSum=self.divSum/self.divWeight

	return self.divSum
end

waterColor={
	init=function ()
		local obj={
			new=rgba(0, 0, 0), last=rgba(0, 0, 0),
			smR=average.init(), smG=average.init(), smB=average.init(), smA=average.init()
		}
		setmetatable(obj, {__index=waterColor})
		return obj
	end
}

function waterColor:clac(v1, v2, smLenRGB, smLenA, mix, bleed, alpha)

	local smLenRGB, smLenA=smLenRGB or 1, smLenA or 1
	local mix, bleed, alpha=mix or 0.5, bleed or 0.5, alpha or 0

	self.new.a=v1.a*(1-alpha)+v2.a/255*v1.a*alpha

	local mixRate=v2.a/255*mix
	local bleedRate=(1-bleed)+v2.a/255*bleed

	if firstDraw then

		self.last.r, self.last.g, self.last.b=v1.r, v1.g, v1.b

		if bleed == 1 then
			self.new.r=v1.r*(1-v2.a/255)+v2.r*(v2.a/255)
			self.new.g=v1.g*(1-v2.a/255)+v2.g*(v2.a/255)
			self.new.b=v1.b*(1-v2.a/255)+v2.b*(v2.a/255)
		else
			self.new.r, self.new.g, self.new.b=v1.r, v1.g, v1.b
		end
	else
		if bleed == 1 then
			self.new.r=self.last.r
			self.new.g=self.last.g
			self.new.b=self.last.b
		else
			self.new.r=v1.r*bleedRate+self.last.r*(1-bleedRate)
			self.new.g=v1.g*bleedRate+self.last.g*(1-bleedRate)
			self.new.b=v1.b*bleedRate+self.last.b*(1-bleedRate)
		end
	end

	if v2.a > 0 then
		self.new.r=self.new.r*(1-mixRate)+v2.r*mixRate
		self.new.g=self.new.g*(1-mixRate)+v2.g*mixRate
		self.new.b=self.new.b*(1-mixRate)+v2.b*mixRate
	end

	self.smR:put(smLenRGB, self.new.r)
	self.smG:put(smLenRGB, self.new.g)
	self.smB:put(smLenRGB, self.new.b)
	self.smA:put(smLenA, self.new.a)

	local draw=rgba(0, 0, 0, 0)

	draw.r=math.floor(self.smR:sum(1))
	draw.g=math.floor(self.smG:sum(1))
	draw.b=math.floor(self.smB:sum(1))
	draw.a=math.floor(self.smA:sum(1))

	self.last.r, self.last.g, self.last.b=draw.r, draw.g, draw.b

	return draw
end

-- main --
function main(x, y, p)

	local w=math.max(bs_width(), 1)

	local interval=w*pm[1].v/100+w*(1-math.min(w, 3)/3)
	local midPosX, midPosY=x, y
	local complementNum=0

	if bs_width_max() < 10 then
		interval=math.max(interval, 0.1)
	end

	local dx, dy=bs_dir()

	if firstDraw then
		water=waterColor.init()
	else
		local distance=bs_distance(lastPosX-x, lastPosY-y)

		if distance < interval then
			return 0
		end

		midPosX=midPosX-distance%interval*dx
		midPosY=midPosY-distance%interval*dy
		complementNum=math.floor(distance/interval)-1
	end

	local circleRndWH=math.random(math.max(50-pm[7].v, 0), 50)/50*(1+math.max(pm[7].v-50, 0)/50)
	local circleW, circleH=w*circleRndWH, w*(1-pm[8].v/100)*circleRndWH

	local splayRange=w*4*((math.max(pm[7].v-50, 0)/50)^3)
	local circleRange=math.max(circleW/2, circleH/2, splayRange+math.min(circleW/2, circleH/2))

	local moveAngle={bs_atan(lastPosX-x, lastPosY-y), bs_atan(dx, dy)+math.rad(360*pm[9].v/100)}

	if pm[9].v == 100 then
		moveAngle[2]=math.rad(bs_grand(0, 180))
	end

	local foreColor=getColor(0)
	local canvasColor=getCanvasColor(x, y, circleRange)

	if pm[10].v > 0 then
		foreColor.a=foreColor.a*(p^(4*pm[10].v/100))
	end

	local wLimit=10

	if bs_width() < wLimit then
		foreColor.a=foreColor.a*(p^((wLimit-bs_width())/wLimit))
	end

	local smLenRGB=math.floor(circleRange*4/interval*pm[4].v/100)
	local smLenAlpha=math.floor(circleRange/(w*math.max(0.2, pm[1].v/100)))

	local draw=water:clac(foreColor, canvasColor, smLenRGB, smLenAlpha, pm[2].v/100, pm[3].v/100, pm[5].v/100)

	draw.r=math.min(draw.r, foreColor.r)
	draw.g=math.min(draw.g, foreColor.g)
	draw.b=math.min(draw.b, foreColor.b)

	local vx, vy=math.cos(moveAngle[1]), math.sin(moveAngle[1])

	local stepNum=brushTip[brushTip.n][1]
	local polygonNum=brushTip[brushTip.n][2]

	stepNum=stepNum+math.floor(stepNum*2*w/2000)
	polygonNum=math.max(polygonNum*math.min(circleW, 20)/20, 1)

	for i=0, complementNum do

		local splayRange=splayRange*math.random()
		local rndAngle=math.rad(bs_grand(0, 180))

		local posX=midPosX+interval*i*vx+splayRange*math.cos(rndAngle)
		local posY=midPosY+interval*i*vy+splayRange*math.sin(rndAngle)

		local stepNumHalf=math.floor(stepNum/2)+1
		local stepAlpha=0

		for j=1, stepNum do

			if j <= stepNumHalf then
				stepAlpha=1.0/stepNumHalf*j
			else
				stepAlpha=1.0/stepNumHalf*(stepNum-j+1)
			end

			for k=1, polygonNum*j do

				local rnd=math.random(brushTip[brushTip.n][3], 100)/100

				local rw=math.max(circleW/(stepNum*2-1)*rnd, 1)
				local rh=math.max(circleH/(stepNum*2-1)*rnd, 1)

				local rx=(circleW/(stepNum*2)*j-rw/2)*math.cos(math.rad(360/polygonNum/j*k))
				local ry=(circleH/(stepNum*2)*j-rh/2)*math.sin(math.rad(360/polygonNum/j*k))

				rx, ry=bs_rotate(rx, ry, moveAngle[2])

				if pm[6].v == 1 then
					bs_ellipse(posX+rx, posY+ry, rw, rw, 0, draw.r, draw.g, draw.b, math.floor(draw.a*stepAlpha))
				else
					bs_ellipse(posX+rx, posY+ry, rw, rw, 0, draw.r, draw.g, draw.b, draw.a)
				end

			end
		end
	end

	lastPosX, lastPosY=midPosX, midPosY
	firstDraw=false

	return 1
end
