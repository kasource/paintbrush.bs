@echo off

REM character code page UTF-8
chcp 65001

for /l %%i in (1,1,4) do (
	echo _iob=%%i > _bristle_%%i.bs
	echo.>> _bristle_%%i.bs
	type bristle.bs >> _bristle_%%i.bs

	echo _iob=%%i > _bristle_darken_%%i.bs
	echo.>> _bristle_darken_%%i.bs
	type bristle_darken.bs >> _bristle_darken_%%i.bs
)
