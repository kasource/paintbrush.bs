--[[
	paintbrush_soft_bg.bs
]]

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"interval", 1, 100, 10},
		{"mix", 0, 100, 50},
		{"drag", 0, 100, 80},
		{"smooth", 1, 100, 50},
		{"background", 0, 100, 100},
		{"rnd-pos", 0, 100, 0},
		{"rnd-size", 0, 100, 0},
		{"flat", 0, 100, 0},
		{"angle", 0, 100, 0},
		{"press-alpha", 0, 100, 50}
	}

	pm.ja={
		"間隔", "混色", "色の引きずり", "補間数", "背景色",
		"ランダム-位置", "ランダム-サイズ", "扁平率", "角度", "筆圧-透明度"
	}

	for i in ipairs(pm) do

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		_G[string.format('param%d', i)]=setParam(pm[i])
		pm[i].v=_G[string.format('bs_param%d', i)]()

	end

	default_size=setParam(50, 0)

	firstDraw=true
	lastPosX, lastPosY=0, 0

-- sub routine --
function rgba(...)
	local t={...}
	local r, g, b, a=t[1] or 0, t[2] or 0, t[3] or 0, t[4] or bs_opaque()*255
	return {r=r, g=g, b=b, a=a}
end

function getColor(rate, a)
	local rate=rate or 0
	local t={bs_forebg(1-math.min(math.max(rate, 0), 1))}
	if a then
		return rgba(t[1], t[2], t[3], math.min(math.max(a, 0), 255))
	else
		return rgba(t[1], t[2], t[3])
	end
end

function getCanvasColor(x, y, radius)

	local pGet=rgba(0, 0, 0, 0)
	local result=rgba(0, 0, 0, 0)

	local moveAngle=bs_atan(bs_dir())
	local pickNum=math.max(90*math.min(bs_width(), 20)/20, 4)
	local pickAngle=180/pickNum

	for i=0, pickNum do

		local cx=x+radius*math.cos(moveAngle+math.rad(pickAngle*i-90))
		local cy=y+radius*math.sin(moveAngle+math.rad(pickAngle*i-90))

		pGet.a=bs_pixel_get_alpha(cx, cy)

		if pGet.a > 0 then
			pGet.r, pGet.g, pGet.b=bs_pixel_get(cx, cy)
			result.r=result.r+pGet.r*pGet.a
			result.g=result.g+pGet.g*pGet.a
			result.b=result.b+pGet.b*pGet.a
			result.a=result.a+pGet.a
		end
	end

	if result.a > 0 then
		result.r=math.floor(result.r/result.a)
		result.g=math.floor(result.g/result.a)
		result.b=math.floor(result.b/result.a)
		result.a=math.floor(result.a/(pickNum+1))
	end

	return result
end

average={
	init=function ()
		local obj={array={}, divSum=0, divWeight=0}
		setmetatable(obj, {__index=average})
		return obj
	end
}

function average:put(num, value)

	if #self.array == 0 then
		for i=1, math.max(num, 1) do
			self.array[i]=value
		end
	else
		table.insert(self.array, value)
		table.remove(self.array, 1)
	end
end

function average:sum(mode, weight)

	if mode < 0 or mode > 2 then
		return nil
	end

	self.divSum, self.divWeight=0, 0

	for i, v in ipairs(self.array) do

		local _weight=1

		if mode == 1 then
			_weight=i
		elseif mode == 2 then
			_weight=i^weight
		end

		self.divSum=self.divSum+v*_weight
		self.divWeight=self.divWeight+_weight
	end

	self.divSum=self.divSum/self.divWeight

	return self.divSum
end

waterColor={
	init=function ()
		local obj={
			new=rgba(0, 0, 0), last=rgba(0, 0, 0),
			smR=average.init(), smG=average.init(), smB=average.init(), smA=average.init()
		}
		setmetatable(obj, {__index=waterColor})
		return obj
	end
}

function waterColor:clac(v1, v2, smLenRGB, smLenA, mix, bleed, alpha)

	local smLenRGB, smLenA=smLenRGB or 1, smLenA or 1
	local mix, bleed, alpha=mix or 0.5, bleed or 0.5, alpha or 0

	self.new.a=v1.a*(1-alpha)+v2.a/255*v1.a*alpha

	local mixRate=v2.a/255*mix
	local bleedRate=(1-bleed)+v2.a/255*bleed

	if firstDraw then

		self.last.r, self.last.g, self.last.b=v1.r, v1.g, v1.b

		if bleed == 1 then
			self.new.r=v1.r*(1-v2.a/255)+v2.r*(v2.a/255)
			self.new.g=v1.g*(1-v2.a/255)+v2.g*(v2.a/255)
			self.new.b=v1.b*(1-v2.a/255)+v2.b*(v2.a/255)
		else
			self.new.r, self.new.g, self.new.b=v1.r, v1.g, v1.b
		end
	else
		if bleed == 1 then
			self.new.r=self.last.r
			self.new.g=self.last.g
			self.new.b=self.last.b
		else
			self.new.r=v1.r*bleedRate+self.last.r*(1-bleedRate)
			self.new.g=v1.g*bleedRate+self.last.g*(1-bleedRate)
			self.new.b=v1.b*bleedRate+self.last.b*(1-bleedRate)
		end
	end

	if v2.a > 0 then
		self.new.r=self.new.r*(1-mixRate)+v2.r*mixRate
		self.new.g=self.new.g*(1-mixRate)+v2.g*mixRate
		self.new.b=self.new.b*(1-mixRate)+v2.b*mixRate
	end

	self.smR:put(smLenRGB, self.new.r)
	self.smG:put(smLenRGB, self.new.g)
	self.smB:put(smLenRGB, self.new.b)
	self.smA:put(smLenA, self.new.a)

	local draw=rgba(0, 0, 0, 0)

	draw.r=math.floor(self.smR:sum(1))
	draw.g=math.floor(self.smG:sum(1))
	draw.b=math.floor(self.smB:sum(1))
	draw.a=math.floor(self.smA:sum(1))

	self.last.r, self.last.g, self.last.b=draw.r, draw.g, draw.b

	return draw
end

-- main --
function main(x, y, p)

	local w=math.max(bs_width(), 1)

	local interval=w*pm[1].v/100
	local midPosX, midPosY=x, y
	local complementNum=0

	if bs_width_max() < 10 then
		interval=math.max(interval, 0.1)
	end

	local dx, dy=bs_dir()

	if firstDraw then
		water=waterColor.init()
	else
		local distance=bs_distance(lastPosX-x, lastPosY-y)

		if distance < interval then
			return 0
		end

		midPosX=midPosX-distance%interval*dx
		midPosY=midPosY-distance%interval*dy
		complementNum=math.floor(distance/interval)-1
	end

	local circleRndWH=1-math.random(0, pm[7].v)/100
	local circleW, circleH=w*circleRndWH, w*(1-pm[8].v/100)*circleRndWH

	local splayRange=w*4*pm[6].v/100
	local circleRange=math.max(circleW/2, circleH/2, splayRange+math.min(circleW/2, circleH/2))

	local moveAngle={bs_atan(lastPosX-x, lastPosY-y), bs_atan(dx, dy)+math.rad(180*pm[9].v/100)}

	if pm[9].v == 100 then
		moveAngle[2]=math.rad(bs_grand(0, 180))
	end

	local canvasColor=getCanvasColor(x, y, circleRange)
	local a=bs_opaque()*255

	if pm[10].v > 0 then
		a=a*(p^(4*pm[10].v/100))
	end

	local wLimit=1

	if bs_width() < wLimit then
		a=a*(p^((wLimit-bs_width())/wLimit))
	end

	local colorRate=pm[5].v/100*(1-canvasColor.a/255*(1-a/255))*(1-a/255)
	local color=getColor(colorRate, a)

	local smLenRGB=math.floor(circleRange*4/interval*pm[4].v/100)
	local smLenAlpha=math.floor(circleRange/(w*math.max(0.2, pm[1].v/100)))

	local draw=water:clac(color, canvasColor, smLenRGB, smLenAlpha, pm[2].v/100, pm[3].v/100)

	local vx, vy=math.cos(moveAngle[1]), math.sin(moveAngle[1])

	for i=0, complementNum do

		local splayRange=splayRange*math.random()
		local rndAngle=math.rad(bs_grand(0, 180))

		local posX=midPosX+interval*i*vx+splayRange*math.cos(rndAngle)
		local posY=midPosY+interval*i*vy+splayRange*math.sin(rndAngle)

		local softNum=12

		for i=0, softNum do

			local w, h=circleW/softNum*i, circleH/softNum*i
			local a=draw.a-draw.a/softNum*i+draw.a/softNum

			bs_ellipse(posX, posY, w, h, moveAngle[2], draw.r, draw.g, draw.b, a)

		end
	end

	lastPosX, lastPosY=midPosX, midPosY
	firstDraw=false

	return 1
end
