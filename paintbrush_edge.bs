--[[
	paintbrush_edge.bs
]]

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"mix", 0, 100, 50},
		{"drag", 0, 100, 80},
		{"smooth", 1, 100, 50},
		{"mask-alpha", 0, 100, 50},
		{"edge-width", 0, 100, 10},
		{"edge-blur", 0, 100, 0},
		{"edge-blot", 0, 100, 0},
		{"noise", 0, 100, 0},
		{"press-size", 0, 100, 25},
		{"press-alpha", 0, 100, 50}
	}

	pm.ja={
		"混色", "色の引きずり", "補間数", "透明マスク", "縁の幅", "縁のぼかし",
		"縁の広がり", "ノイズ", "筆圧-サイズ", "筆圧-透明度"
	}

	for i in ipairs(pm) do

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		_G[string.format('param%d', i)]=setParam(pm[i])
		pm[i].v=_G[string.format('bs_param%d', i)]()

	end

	default_size=setParam(50, 0)

	firstDraw=true
	lastPosX, lastPosY=0, 0

-- sub routine --
function rgba(...)
	local t={...}
	local r, g, b, a=t[1] or 0, t[2] or 0, t[3] or 0, t[4] or bs_opaque()*255
	return {r=r, g=g, b=b, a=a}
end

function getColor(rate, a)
	local rate=rate or 0
	local t={bs_forebg(1-math.min(math.max(rate, 0), 1))}
	if a then
		return rgba(t[1], t[2], t[3], math.min(math.max(a, 0), 255))
	else
		return rgba(t[1], t[2], t[3])
	end
end

function getCanvasColor(x, y, radius)

	local pGet=rgba(0, 0, 0, 0)
	local result=rgba(0, 0, 0, 0)

	local moveAngle=bs_atan(bs_dir())
	local pickNum=math.max(90*math.min(bs_width(), 20)/20, 4)
	local pickAngle=180/pickNum

	for i=0, pickNum do

		local cx=x+radius*math.cos(moveAngle+math.rad(pickAngle*i-90))
		local cy=y+radius*math.sin(moveAngle+math.rad(pickAngle*i-90))

		pGet.a=bs_pixel_get_alpha(cx, cy)

		if pGet.a > 0 then
			pGet.r, pGet.g, pGet.b=bs_pixel_get(cx, cy)
			result.r=result.r+pGet.r*pGet.a
			result.g=result.g+pGet.g*pGet.a
			result.b=result.b+pGet.b*pGet.a
			result.a=result.a+pGet.a
		end
	end

	if result.a > 0 then
		result.r=math.floor(result.r/result.a)
		result.g=math.floor(result.g/result.a)
		result.b=math.floor(result.b/result.a)
		result.a=math.floor(result.a/(pickNum+1))
	end

	return result
end

average={
	init=function ()
		local obj={array={}, divSum=0, divWeight=0}
		setmetatable(obj, {__index=average})
		return obj
	end
}

function average:put(num, value)

	if #self.array == 0 then
		for i=1, math.max(num, 1) do
			self.array[i]=value
		end
	else
		table.insert(self.array, value)
		table.remove(self.array, 1)
	end
end

function average:sum(mode, weight)

	if mode < 0 or mode > 2 then
		return nil
	end

	self.divSum, self.divWeight=0, 0

	for i, v in ipairs(self.array) do

		local _weight=1

		if mode == 1 then
			_weight=i
		elseif mode == 2 then
			_weight=i^weight
		end

		self.divSum=self.divSum+v*_weight
		self.divWeight=self.divWeight+_weight
	end

	self.divSum=self.divSum/self.divWeight

	return self.divSum
end

waterColor={
	init=function ()
		local obj={
			new=rgba(0, 0, 0), last=rgba(0, 0, 0),
			smR=average.init(), smG=average.init(), smB=average.init(), smA=average.init()
		}
		setmetatable(obj, {__index=waterColor})
		return obj
	end
}

function waterColor:clac(v1, v2, smLenRGB, smLenA, mix, bleed, alpha)

	local smLenRGB, smLenA=smLenRGB or 1, smLenA or 1
	local mix, bleed, alpha=mix or 0.5, bleed or 0.5, alpha or 0

	self.new.a=v1.a*(1-alpha)+v2.a/255*v1.a*alpha

	local mixRate=v2.a/255*mix
	local bleedRate=(1-bleed)+v2.a/255*bleed

	if firstDraw then

		self.last.r, self.last.g, self.last.b=v1.r, v1.g, v1.b

		if bleed == 1 then
			self.new.r=v1.r*(1-v2.a/255)+v2.r*(v2.a/255)
			self.new.g=v1.g*(1-v2.a/255)+v2.g*(v2.a/255)
			self.new.b=v1.b*(1-v2.a/255)+v2.b*(v2.a/255)
		else
			self.new.r, self.new.g, self.new.b=v1.r, v1.g, v1.b
		end
	else
		if bleed == 1 then
			self.new.r=self.last.r
			self.new.g=self.last.g
			self.new.b=self.last.b
		else
			self.new.r=v1.r*bleedRate+self.last.r*(1-bleedRate)
			self.new.g=v1.g*bleedRate+self.last.g*(1-bleedRate)
			self.new.b=v1.b*bleedRate+self.last.b*(1-bleedRate)
		end
	end

	if v2.a > 0 then
		self.new.r=self.new.r*(1-mixRate)+v2.r*mixRate
		self.new.g=self.new.g*(1-mixRate)+v2.g*mixRate
		self.new.b=self.new.b*(1-mixRate)+v2.b*mixRate
	end

	self.smR:put(smLenRGB, self.new.r)
	self.smG:put(smLenRGB, self.new.g)
	self.smB:put(smLenRGB, self.new.b)
	self.smA:put(smLenA, self.new.a)

	local draw=rgba(0, 0, 0, 0)

	draw.r=math.floor(self.smR:sum(1))
	draw.g=math.floor(self.smG:sum(1))
	draw.b=math.floor(self.smB:sum(1))
	draw.a=math.floor(self.smA:sum(1))

	self.last.r, self.last.g, self.last.b=draw.r, draw.g, draw.b

	return draw
end

-- main --
function main(x, y, p)

	local wMin=bs_width_min()
	local wMax=bs_width_max()
	local opacity=bs_opaque()

	local w=wMax

	if pm[9].v > 0 then
		w=w*(p^(4*pm[9].v/100)*(1-wMin/wMax)+wMin/wMax)
	end

	w=math.max(w, 1)

	local interval=w*0.08+w*0.05*(pm[6].v/100+pm[5].v*pm[7].v/10000)+w*0.1*(1-opacity)*(1-w/wMax)+w*0.3*(pm[5].v*pm[6].v*pm[7].v/(100^3))
	local midPosX, midPosY=x, y
	local complementNum=0

	if wMax < 10 then
		interval=math.max(interval, 1)
	end

	local dx, dy=bs_dir()

	if firstDraw then
		water={}
		for i=1, 3 do
			water[i]=waterColor.init()
		end
	else
		local distance=bs_distance(lastPosX-x, lastPosY-y)

		if distance < interval then
			return 0
		end

		midPosX=midPosX-distance%interval*dx
		midPosY=midPosY-distance%interval*dy
		complementNum=math.floor(distance/interval)-1
	end

	local moveAngle={bs_atan(lastPosX-x, lastPosY-y), bs_atan(dx, dy)}

	local canvasColor=getCanvasColor(x, y, w/2)
	local foreColor=getColor(0, opacity*255)

	if pm[10].v > 0 then
		foreColor.a=foreColor.a*(p^(4*pm[10].v/100))
	end

	local smLenRGB=math.floor(w*4/interval*pm[3].v/100)
	local smLenAlpha=math.floor(w/(w*math.max(0.2, interval/w)))
	local edgeMix=math.max(pm[1].v/100, 0.5)

	local draw={}

	draw[1]=water[1]:clac(foreColor, canvasColor, smLenRGB, smLenAlpha, pm[1].v/100, pm[2].v/100, pm[4].v/100)
	draw[2]=water[2]:clac(draw[1], canvasColor, smLenRGB*math.max(1-pm[2].v/100, 0.5), 1, edgeMix, pm[2].v/100*edgeMix, 0)

	draw[2].r=math.min(draw[1].r, draw[2].r)
	draw[2].g=math.min(draw[1].g, draw[2].g)
	draw[2].b=math.min(draw[1].b, draw[2].b)

	draw[3]=water[3]:clac(draw[1], canvasColor, smLenRGB, 1, math.random(), pm[2].v/100*edgeMix, 0)

	local edgeDist=w/2*(1-pm[5].v/100/2)-(1-pm[5].v/100)
	local edgeW, edgeH=w/2, w/2*pm[5].v/100

	local vx, vy=math.cos(moveAngle[1]), math.sin(moveAngle[1])

	for i=0, complementNum do

		local posX=midPosX+interval*i*vx
		local posY=midPosY+interval*i*vy

		if not firstDraw then

			-- base --
			local softNum=6
			local w=w

			if pm[5].v == 0 then
				softNum=8
				w=w*(math.random(100-pm[7].v, 100)/100*2-(1-pm[7].v/100))
			end

			softNum=softNum*(pm[5].v*pm[7].v/10000*(1-pm[6].v/100)+pm[6].v/100)
			softNum=math.max(math.ceil(softNum), 3)

			for i=1, softNum do

				local ii=i/softNum
				local ew=pm[5].v/100

				local ww=w*(ii^(ew*(pm[6].v/100+pm[7].v/100)))

				if pm[5].v == 0 then
					ww=w*(ii^(pm[6].v/100))
				end

				local aa=draw[1].a*(1-pm[5].v/100*opacity/2)
				aa=math.ceil(aa-aa/softNum*i+aa/softNum)

				bs_ellipse(posX, posY, ww, ww, moveAngle[2], draw[1].r, draw[1].g, draw[1].b, aa)

			end

			-- edge --
			if pm[5].v > 0 then

				local cnt=math.floor(3*pm[7].v/100*(pm[5].v/100)*(1-pm[6].v/100))+1

				softNum=math.max(8*pm[6].v/100, 1)

				for j=1, cnt do

					local rndAngle=math.rad(bs_grand(0, 180))
					local rx, ry=math.cos(rndAngle), math.sin(rndAngle)

					local edgeBlotDist=math.random(0, w/8*(0.5+pm[5].v/100/2)*pm[7].v/100)*w/wMax
					local edgeBlotRndWH=1+math.random()*pm[7].v/100
					local edgeRndA=1-math.random()*pm[7].v/100/2+(1-pm[5].v/100/2)

					local edgeW=math.min(edgeW/cnt*edgeBlotRndWH*(1-pm[7].v/100*(1-pm[5].v/100)/2), w)
					local edgeH=edgeH*edgeBlotRndWH

					local nx, ny=bs_normal()

					local xx, yy=edgeDist*nx+edgeBlotDist*rx, edgeDist*ny+edgeBlotDist*ry
					local a=draw[2].a*(1-math.random(pm[7].v, 100)/100*0.2)*(1-w/wMax*0.3)

					for i=1, softNum do

						local ii=i/softNum

						local e1=ii^(pm[6].v/100*w/wMax*0.8)
						local e3=1+((pm[5].v*pm[6].v*pm[7].v)/(100^3))

						local edgeW, edgeH=edgeW*e1*e3, edgeH*e1*e3

						local aa=draw[2].a^(1.2-pm[6].v/100*0.2)
						aa=aa-aa/softNum*i+aa/softNum
						aa=math.min(math.ceil(aa), 255)

						local an=math.pi/4*((pm[5].v*pm[6].v*pm[7].v)/(100^3))

						bs_ellipse(posX-xx, posY-yy, edgeW, edgeH, moveAngle[2]+bs_grand(0, an), draw[2].r, draw[2].g, draw[2].b, aa)
						bs_ellipse(posX+xx, posY+yy, edgeW, edgeH, moveAngle[2]+bs_grand(0, an), draw[2].r, draw[2].g, draw[2].b, aa)

					end
				end
			end

			-- noise --
			cnt=math.ceil(15*pm[8].v/100*w/wMax)

			for k=1, cnt do

				draw[3]=water[3]:clac(draw[1], canvasColor, smLenRGB, 1, math.random(), pm[2].v/100*edgeMix, 0)

				local function diff(a, b)
					if a <= math.random(0, 255) then
						return math.min(a, b)
					else
						return math.max(a, b)
					end
				end

				draw[3].r=diff(draw[3].r, draw[1].r)
				draw[3].g=diff(draw[3].g, draw[1].g)
				draw[3].b=diff(draw[3].b, draw[1].b)

				local rndAngle=bs_grand(0, math.pi)
				local rx, ry=math.cos(rndAngle), math.sin(rndAngle)
				local noiseW=w/8*math.random(pm[6].v, 100)/100

				local xx, yy=(w-noiseW)/2*rx*math.random(), (w-noiseW)/2*ry*math.random()
				local a=math.floor(draw[3].a^(1.65-pm[6].v/1000+pm[5].v/1000)*math.random())

				noiseW=(math.min(((w-noiseW)/8)/bs_distance(xx, yy), 1)^(pm[6].v/100))*noiseW
				a=a*math.random(100-pm[6].v*(1-pm[5].v*pm[7].v/10000), 100)/100

				rec[rec.cnt]={x=posX+xx, y=posY+yy, w=noiseW, r=draw[3].r, g=draw[3].g, b=draw[3].b, a=a}
				rec.cnt=rec.cnt+1

				-- bs_ellipse(posX+xx, posY+yy, noiseW, noiseW, 0, draw[3].r, draw[3].g, draw[3].b, a)

			end
		end
	end

	lastPosX, lastPosY=midPosX, midPosY
	firstDraw=false

	return 1
end

rec={x=0, y=0, w=0, r=0, g=0, b=0, a=0, cnt=1}

function last(x, y, p)
	for i, v in ipairs(rec) do
		bs_ellipse(v.x, v.y, v.w, v.w, 0, v.r, v.g, v.b, v.a)
	end
end